package mygamelib

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

type CreateGameRequest struct {
	Name string `json:"name"`
}

type CreatePlayerRequest struct {
	Name string `json:"name"`
}

type DealCardRequest struct {
	Count uint32 `json:"count"`
}

type GetPlayerCardResponse struct {
	Cards []string `json:"cards"`
}

type GetPlayerListEntry struct {
	Id             string `json:"id"`
	Name           string `json:"name"`
	TotalCardValue int    `json:"totalCardValue"`
}

type GetPlayerListResponse struct {
	Players []*GetPlayerListEntry `json:"players"`
}

type CountPerSuitResponse struct {
	CountPerSuit map[string]int `json:"countPerSuit"`
}

type CountCardResponse struct {
	CardCount []*DeckCardCount `json:"cardCount"`
}

func (server *Server) InitializeRoutes(router gin.IRouter) {
	router.POST("/games/:gameId", server.handleCreateGame)
	router.PUT("/games/:gameId", server.handleCreateGame)
	router.DELETE("/games/:gameId", server.handleDeleteGame)
	router.POST("/decks/:deckId", server.handleCreateDeck)
	router.PUT("/decks/:deckId", server.handleCreateDeck)
	router.POST("/players/:playerId", server.handleCreatePlayer)
	router.PUT("/players/:playerId", server.handleCreatePlayer)
	router.DELETE("/players/:playerId", server.handleDeletePlayer)
	router.POST("/games/:gameId/decks/:deckId", server.handleAddDeckToGame)
	router.POST("/games/:gameId/players/:playerId", server.handleAddPlayerToGame)
	router.DELETE("/games/:gameId/players/:playerId", server.handleRemovePlayerFromGame)
	router.POST("/games/:gameId/players/:playerId/deal-cards", server.handleDealCards)
	router.GET("/games/:gameId/players/:playerId/cards", server.handleGetPlayerCards)
	router.GET("/games/:gameId/players", server.handleGetPlayerList)
	router.GET("/games/:gameId/count-undealt-per-suit", server.handleCountUndealtPerSuit)
	router.GET("/games/:gameId/count-undealt-cards", server.handleCountUndealtCards)
	router.POST("/games/:gameId/shuffle-cards", server.handleShuffleCards)
}

func buildErrorResponse(c *gin.Context, err error) {
	httpStatus := http.StatusBadRequest
	if _, is404 := err.(*NotFoundError); is404 {
		httpStatus = http.StatusNotFound
	}
	if _, is409 := err.(*AlreadyExistsError); is409 {
		httpStatus = http.StatusConflict
	}
	c.IndentedJSON(httpStatus, map[string]string{
		"message": err.Error(),
	})
}

func (server *Server) handleCreateGame(c *gin.Context) {
	createGameRequest := CreateGameRequest{}
	if err := c.BindJSON(&createGameRequest); err != nil {
		buildErrorResponse(c, errors.New("invalid json"))
		return
	}

	if createGameRequest.Name == "" {
		buildErrorResponse(c, errors.New("missing 'name' field from request"))
		return
	}
	if err := server.CreateGame(c.Param("gameId"), createGameRequest.Name, c.Request.Method == "PUT"); err != nil {
		buildErrorResponse(c, err)
		return
	}

	c.Status(http.StatusCreated)
}

func (server *Server) handleDeleteGame(c *gin.Context) {
	if err := server.DeleteGame(c.Param("gameId")); err != nil {
		buildErrorResponse(c, err)
		return
	}

	c.Status(http.StatusNoContent)
}

func (server *Server) handleCreateDeck(c *gin.Context) {
	if err := server.CreateDeck(c.Param("deckId"), c.Request.Method == "PUT"); err != nil {
		buildErrorResponse(c, err)
		return
	}
	c.Status(http.StatusCreated)
}

func (server *Server) handleCreatePlayer(c *gin.Context) {
	createPlayerRequest := CreatePlayerRequest{}
	if err := c.BindJSON(&createPlayerRequest); err != nil {
		buildErrorResponse(c, errors.New("invalid json"))
		return
	}

	if createPlayerRequest.Name == "" {
		buildErrorResponse(c, errors.New("missing 'name' field from request"))
		return
	}

	if err := server.CreatePlayer(c.Param("playerId"), createPlayerRequest.Name, c.Request.Method == "PUT"); err != nil {
		buildErrorResponse(c, err)
		return
	}
	c.Status(http.StatusCreated)
}

func (server *Server) handleDeletePlayer(c *gin.Context) {
	if err := server.DeletePlayer(c.Param("playerId")); err != nil {
		buildErrorResponse(c, err)
		return
	}

	c.Status(http.StatusNoContent)
}

func (server *Server) handleAddDeckToGame(c *gin.Context) {
	if err := server.AddDeckToGame(c.Param("gameId"), c.Param("deckId")); err != nil {
		buildErrorResponse(c, err)
		return
	}
	c.Status(http.StatusNoContent)
}

func (server *Server) handleAddPlayerToGame(c *gin.Context) {
	if err := server.AddPlayerToGame(c.Param("gameId"), c.Param("playerId")); err != nil {
		buildErrorResponse(c, err)
		return
	}
	c.Status(http.StatusNoContent)
}

func (server *Server) handleRemovePlayerFromGame(c *gin.Context) {
	if err := server.RemovePlayerFromGame(c.Param("gameId"), c.Param("playerId")); err != nil {
		buildErrorResponse(c, err)
		return
	}
	c.Status(http.StatusNoContent)
}

func (server *Server) handleDealCards(c *gin.Context) {
	dealCardRequest := DealCardRequest{
		Count: 1,
	}
	if err := c.BindJSON(&dealCardRequest); err != nil {
		buildErrorResponse(c, errors.New("invalid json"))
		return
	}

	if err := server.DealCards(c.Param("gameId"), c.Param("playerId"), dealCardRequest.Count); err != nil {
		buildErrorResponse(c, err)
		return
	}
	c.Status(http.StatusNoContent)
}

func (server *Server) handleGetPlayerCards(c *gin.Context) {
	cards, err := server.GetPlayerCards(c.Param("gameId"), c.Param("playerId"))
	if err != nil {
		buildErrorResponse(c, err)
		return
	}
	getPlayerCardResponse := GetPlayerCardResponse{}
	getPlayerCardResponse.Cards = make([]string, len(cards))
	for index, card := range cards {
		getPlayerCardResponse.Cards[index] = card.Code()
	}
	c.IndentedJSON(http.StatusOK, &getPlayerCardResponse)
}

func (server *Server) handleGetPlayerList(c *gin.Context) {
	playerList, scoreByPlayers, err := server.GetPlayerList(c.Param("gameId"))
	if err != nil {
		buildErrorResponse(c, err)
		return
	}
	getPlayerListResponse := GetPlayerListResponse{}
	getPlayerListResponse.Players = make([]*GetPlayerListEntry, len(playerList))
	for i, player := range playerList {
		getPlayerListResponse.Players[i] = &GetPlayerListEntry{
			Id:             player.Id(),
			Name:           player.Name(),
			TotalCardValue: scoreByPlayers[player],
		}
	}
	c.IndentedJSON(http.StatusOK, &getPlayerListResponse)
}

func (server *Server) handleCountUndealtPerSuit(c *gin.Context) {
	countPerSuit, err := server.CountUndealtPerSuit(c.Param("gameId"))
	if err != nil {
		buildErrorResponse(c, err)
		return
	}
	countPerSuitResponse := CountPerSuitResponse{}
	countPerSuitResponse.CountPerSuit = countPerSuit
	c.IndentedJSON(http.StatusOK, &countPerSuitResponse)
}

func (server *Server) handleCountUndealtCards(c *gin.Context) {
	cardCountAgg, err := server.CountUndealtCards(c.Param("gameId"))
	if err != nil {
		buildErrorResponse(c, err)
		return
	}
	countCardResponse := CountCardResponse{}
	countCardResponse.CardCount = cardCountAgg
	c.IndentedJSON(http.StatusOK, &countCardResponse)
}

func (server *Server) handleShuffleCards(c *gin.Context) {
	if err := server.ShuffleCards(c.Param("gameId")); err != nil {
		buildErrorResponse(c, err)
		return
	}
	c.Status(http.StatusNoContent)
}
