package mygamelib

import (
	"crypto/rand"
	"encoding/binary"
)

// Provides an implementation of a source taken by math/rand's New function.
type CryptoSource64 struct {
}

func (cryptoRandom *CryptoSource64) Seed(seed int64) {
}

func (cryptoRandom *CryptoSource64) Int63() int64 {
	var value int64
	binary.Read(rand.Reader, binary.LittleEndian, &value)
	return value & (1<<63 - 1)
}
