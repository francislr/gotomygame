package mygamelib

import "regexp"

var CardRanks []string = []string{
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
	"10",
	"J",
	"Q",
	"K",
	"A",
}

// Card suits are ordered according to documentation
// (hearts, spades, clubs, and diamonds)
var CardSuits []string = []string{
	"H",
	"S",
	"C",
	"D",
}

var CardValueMap map[string]int = map[string]int{
	"A":  1,
	"2":  2,
	"3":  3,
	"4":  4,
	"5":  5,
	"6":  6,
	"7":  7,
	"8":  8,
	"9":  9,
	"10": 10,
	"J":  11,
	"Q":  12,
	"K":  13,
}

var cardCodePattern = "^([2-9]|10|J|Q|K|A)(C|D|H|S)$"
var cardCodeRegex *regexp.Regexp

func getCardCodeRegex() *regexp.Regexp {
	if cardCodeRegex == nil {
		cardCodeRegex = regexp.MustCompile(cardCodePattern)
	}
	return cardCodeRegex
}
