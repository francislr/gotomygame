package mygamelib

import (
	"errors"
	"fmt"
	rng "math/rand"
	"strings"
)

type Player struct {
	id    string
	name  string
	games map[*Game]bool
}

func NewPlayer(id string, name string) *Player {
	return &Player{
		id:    id,
		name:  name,
		games: make(map[*Game]bool, 0),
	}
}

func (player *Player) Id() string {
	return player.id
}

func (player *Player) Name() string {
	return player.name
}

func (player *Player) SetName(name string) {
	player.name = name
}

func (player *Player) Games() []*Game {
	games := make([]*Game, len(player.games))
	i := 0
	for k := range player.games {
		games[i] = k
		i = i + 1
	}
	return games
}

func (player *Player) String() string {
	return player.name
}

type Card struct {
	// The first character is the rank and the second is the suit
	// Possible code for faces
	// J = Jack, Q = Queen, K = King, and A = Ace
	code string
}

func NewCard(code string) (*Card, error) {
	re := getCardCodeRegex()
	match := re.MatchString(code)
	if !match {
		return nil, fmt.Errorf("the card '%s' is not a valid card code", code)
	}
	return &Card{
		code,
	}, nil
}

func (card *Card) Code() string {
	return card.code
}

func (card *Card) Rank() string {
	re := getCardCodeRegex()
	result := re.FindStringSubmatch(card.code)
	return result[1]
}

func (card *Card) Suit() string {
	re := getCardCodeRegex()
	result := re.FindStringSubmatch(card.code)
	return result[2]
}

func (card *Card) Value() int {
	rank := card.Rank()
	return CardValueMap[rank]
}

type Deck struct {
	id    string
	cards []*Card
	game  *Game
}

func NewDeck(id string, cards []*Card) *Deck {
	return &Deck{
		id:    id,
		cards: cards,
		game:  nil,
	}
}

func NewDeckWith52Cards(id string) (*Deck, error) {
	cards := make([]*Card, 52)
	index := 0
	for _, rank := range CardRanks {
		for _, suit := range CardSuits {
			code := fmt.Sprintf("%s%s", rank, suit)
			card, err := NewCard(code)
			if err != nil {
				return nil, err
			}
			cards[index] = card
			index++
		}
	}
	return NewDeck(id, cards), nil
}

func (deck *Deck) Id() string {
	return deck.id
}

func (deck *Deck) Cards() []*Card {
	return deck.cards
}

// Return the value sum of all card ranks
func (deck *Deck) SumValue() int {
	total := 0
	for _, card := range deck.cards {
		total = total + card.Value()
	}
	return total
}

// Return which game the deck belongs to
func (deck *Deck) Game() *Game {
	return deck.game
}

// Add a card at the end of the deck
func (deck *Deck) AppendCard(card *Card) {
	deck.cards = append(deck.cards, card)
}

// Remove and return the last card from the deck
func (deck *Deck) PopCard() *Card {
	if len(deck.cards) == 0 {
		return nil
	}
	lastCard := deck.cards[len(deck.cards)-1]
	deck.cards = deck.cards[:len(deck.cards)-1]
	return lastCard
}

// Number of cards per suit in the deck
func (deck *Deck) CountPerSuit() map[string]int {
	countPerSuit := make(map[string]int, len(CardSuits))
	for _, suit := range CardSuits {
		countPerSuit[suit] = 0
	}
	for _, card := range deck.cards {
		countPerSuit[card.Suit()]++
	}
	return countPerSuit
}

// Number of cards per rank in the deck
func (deck *Deck) CountPerRank() map[string]int {
	countPerRank := make(map[string]int, len(CardRanks))
	for _, rank := range CardRanks {
		countPerRank[rank] = 0
	}
	for _, card := range deck.cards {
		countPerRank[card.Rank()]++
	}
	return countPerRank
}

// Return a string which is composed of all cards separated with a comma
func (deck *Deck) String() string {
	cardCodes := make([]string, len(deck.cards))
	for index, card := range deck.cards {
		cardCodes[index] = card.Code()
	}
	return strings.Join(cardCodes, ", ")
}

type Game struct {
	id      string
	name    string
	decks   []*Deck
	players []*Player
	hands   map[*Player]*Deck
}

func NewGame(id string, name string) *Game {
	return &Game{
		id:      id,
		name:    name,
		decks:   make([]*Deck, 0),
		players: make([]*Player, 0),
		hands:   make(map[*Player]*Deck),
	}
}

func (game *Game) Name() string {
	return game.name
}

func (game *Game) SetName(name string) {
	game.name = name
}

func (game *Game) AddDeck(deck *Deck) error {
	if deck.game != nil {
		return errors.New("the deck is already assigned to a game")
	}
	deck.game = game
	game.decks = append(game.decks, deck)
	return nil
}

func (game *Game) Players() []*Player {
	return game.players
}

func (game *Game) AddPlayer(player *Player) error {
	index := game.findPlayerByIndex(player)
	if index != -1 {
		return fmt.Errorf("the player '%s' is already in the game", player)
	}
	player.games[game] = true
	game.players = append(game.players, player)
	game.hands[player] = NewDeck(player.id, make([]*Card, 0))
	return nil
}

func (game *Game) RemovePlayer(player *Player) error {
	index := game.findPlayerByIndex(player)
	if index == -1 {
		return fmt.Errorf("the player '%s' is absent from the game", player)
	}
	if index != -1 {
		delete(player.games, game)
		game.players = append(game.players[:index], game.players[index+1:]...)
		delete(game.hands, player)
	}
	return nil
}

func (game *Game) GetPlayerDeck(player *Player) *Deck {
	playerDeck, exists := game.hands[player]
	if !exists {
		return nil
	}
	return playerDeck
}

// Number of cards per suit in all undealt decks
func (game *Game) CountUndealtPerSuit() map[string]int {
	countPerSuit := make(map[string]int, len(CardSuits))
	for _, suit := range CardSuits {
		countPerSuit[suit] = 0
	}
	for _, deck := range game.decks {
		for suit, count := range deck.CountPerSuit() {
			countPerSuit[suit] = countPerSuit[suit] + count
		}
	}
	return countPerSuit
}

// Number of cards per suit and rank in all undealt decks
func (game *Game) CountUndealtCards() map[string]int {
	countAgg := make(map[string]int, len(CardRanks)*len(CardSuits))
	for _, rank := range CardRanks {
		for _, suit := range CardSuits {
			code := fmt.Sprintf("%s%s", rank, suit)
			countAgg[code] = 0
		}
	}
	for _, deck := range game.decks {
		for _, card := range deck.Cards() {
			countAgg[card.Code()]++
		}
	}
	return countAgg
}

// Pop a card from the next non-empty deck respecting the order they were added.
func (game *Game) PopCard() *Card {
	var card *Card
	for _, deck := range game.decks {
		card = deck.PopCard()
		if card != nil {
			break
		}
	}
	return card
}

// Shuffle all decks added to game
func (game *Game) ShuffleDecks(randomGenerator *rng.Rand) {
	for _, deck := range game.decks {
		deck.Shuffle(randomGenerator)
	}
}

func (game *Game) findPlayerByIndex(player *Player) int {
	var index int = -1
	for i, playerEntry := range game.players {
		if playerEntry.Id() == player.Id() {
			index = i
			break
		}
	}
	return index
}

func (game *Game) String() string {
	return game.name
}
