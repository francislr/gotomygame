package mygamelib

import (
	"fmt"
	"math/rand"
	"sort"
)

type RandomGeneratorFactory func() *rand.Rand

type Server struct {
	players                map[string]*Player
	decks                  map[string]*Deck
	games                  map[string]*Game
	randomGeneratorFactory RandomGeneratorFactory
}

type DeckCardCount struct {
	Card  string `json:"card"`
	Suit  string `json:"suit"`
	Rank  string `json:"rank"`
	Count int    `json:"count"`
}

type NotFoundError struct {
	objectType string
	objectId   string
}

func newNotFoundError(objectType string, objectId string) *NotFoundError {
	return &NotFoundError{objectType, objectId}
}

func (err *NotFoundError) Error() string {
	return fmt.Sprintf("the object '%s' of type '%s' does not exist", err.objectId, err.objectType)
}

type AlreadyExistsError struct {
	objectType string
	objectId   string
}

func newAlreadyExistsError(objectType string, objectId string) *AlreadyExistsError {
	return &AlreadyExistsError{objectType, objectId}
}

func (err *AlreadyExistsError) Error() string {
	return fmt.Sprintf("the object '%s' of type '%s' already exists", err.objectId, err.objectType)
}

func NewServer(randomGeneratorFactory RandomGeneratorFactory) *Server {
	return &Server{
		players:                make(map[string]*Player),
		decks:                  make(map[string]*Deck),
		games:                  make(map[string]*Game),
		randomGeneratorFactory: randomGeneratorFactory,
	}
}

func (server *Server) CreateGame(gameId string, name string, overwrite bool) error {
	game, exists := server.games[gameId]
	if exists && !overwrite {
		return newAlreadyExistsError("game", gameId)
	}
	if !exists {
		game = NewGame(gameId, name)
	}
	game.SetName(name)
	server.games[gameId] = game
	return nil
}

func (server *Server) DeleteGame(gameId string) error {
	_, exists := server.games[gameId]
	if !exists {
		return newNotFoundError("game", gameId)
	}
	delete(server.games, gameId)
	return nil
}

func (server *Server) CreateDeck(deckId string, overwrite bool) error {
	var deck *Deck
	deck, exists := server.decks[deckId]
	if exists && !overwrite {
		return newAlreadyExistsError("deck", deckId)
	}
	if !exists {
		var err error
		deck, err = NewDeckWith52Cards(deckId)
		if err != nil {
			return err
		}
	}

	server.decks[deck.id] = deck
	return nil
}

func (server *Server) CreatePlayer(playerId string, name string, overwrite bool) error {
	player, exists := server.players[playerId]
	if exists && !overwrite {
		return newAlreadyExistsError("player", playerId)
	}
	if !exists {
		player = NewPlayer(playerId, name)
	}
	player.SetName(name)
	server.players[playerId] = player
	return nil
}

func (server *Server) DeletePlayer(playerId string) error {
	player, exists := server.players[playerId]
	if !exists {
		return newNotFoundError("player", playerId)
	}
	for _, game := range player.Games() {
		game.RemovePlayer(player)
	}
	delete(server.players, playerId)
	return nil
}

func (server *Server) AddDeckToGame(gameId string, deckId string) error {
	game, exists := server.games[gameId]
	if !exists {
		return newNotFoundError("game", gameId)
	}
	deck, exists := server.decks[deckId]
	if !exists {
		return newNotFoundError("deck", deckId)
	}
	if deck.Game() != nil {
		return fmt.Errorf("the deck '%s' is already assigned to a game", deckId)
	}
	return game.AddDeck(deck)
}

func (server *Server) AddPlayerToGame(gameId string, playerId string) error {
	game, exists := server.games[gameId]
	if !exists {
		return newNotFoundError("game", gameId)
	}
	player, exists := server.players[playerId]
	if !exists {
		return newNotFoundError("player", playerId)
	}
	return game.AddPlayer(player)
}

func (server *Server) RemovePlayerFromGame(gameId string, playerId string) error {
	game, exists := server.games[gameId]
	if !exists {
		return newNotFoundError("game", gameId)
	}
	player, exists := server.players[playerId]
	if !exists {
		return newNotFoundError("player", playerId)
	}
	return game.RemovePlayer(player)
}

func (server *Server) DealCards(gameId string, playerId string, count uint32) error {
	game, exists := server.games[gameId]
	if !exists {
		return newNotFoundError("game", gameId)
	}
	player, exists := server.players[playerId]
	if !exists {
		return newNotFoundError("player", playerId)
	}
	playerDeck := game.GetPlayerDeck(player)
	if playerDeck == nil {
		return fmt.Errorf("the player '%s' is not in the game '%s'", playerId, gameId)
	}
	for i := uint32(0); i < count; i++ {
		card := game.PopCard()
		if card == nil {
			break
		}
		playerDeck.AppendCard(card)
	}

	return nil
}

func (server *Server) GetPlayerCards(gameId string, playerId string) ([]*Card, error) {
	game, exists := server.games[gameId]
	if !exists {
		return nil, newNotFoundError("game", gameId)
	}
	player, exists := server.players[playerId]
	if !exists {
		return nil, newNotFoundError("player", playerId)
	}
	playerDeck := game.GetPlayerDeck(player)
	if playerDeck == nil {
		return nil, fmt.Errorf("the player '%s' is not in the game '%s'", playerId, gameId)
	}

	return playerDeck.Cards(), nil
}

func (server *Server) GetPlayerList(gameId string) ([]*Player, map[*Player]int, error) {
	game, exists := server.games[gameId]
	if !exists {
		return nil, nil, newNotFoundError("game", gameId)
	}
	playerList := game.Players()
	totalValueByPlayers := make(map[*Player]int, len(playerList))
	for _, player := range playerList {
		playerDeck := game.GetPlayerDeck(player)
		totalValueByPlayers[player] = playerDeck.SumValue()
	}
	sort.SliceStable(playerList, func(i int, j int) bool {
		return totalValueByPlayers[playerList[i]] > totalValueByPlayers[playerList[j]]
	})

	return playerList, totalValueByPlayers, nil
}

func (server *Server) CountUndealtCards(gameId string) ([]*DeckCardCount, error) {
	game, exists := server.games[gameId]
	if !exists {
		return nil, newNotFoundError("game", gameId)
	}

	// Sort card ranks from high to low value
	cardRanks := CardRanks
	sort.SliceStable(cardRanks, func(i int, j int) bool {
		return CardValueMap[cardRanks[i]] > CardValueMap[cardRanks[j]]
	})

	cardCountAgg := game.CountUndealtCards()
	deckCardCounts := make([]*DeckCardCount, len(cardRanks)*len(CardSuits))
	for i, suit := range CardSuits {
		for j, rank := range cardRanks {
			code := fmt.Sprintf("%s%s", rank, suit)
			deckCardCounts[i*len(cardRanks)+j] = &DeckCardCount{
				Card:  code,
				Suit:  suit,
				Rank:  rank,
				Count: cardCountAgg[code],
			}
		}

	}

	return deckCardCounts, nil
}

func (server *Server) CountUndealtPerSuit(gameId string) (map[string]int, error) {
	game, exists := server.games[gameId]
	if !exists {
		return nil, newNotFoundError("game", gameId)
	}

	return game.CountUndealtPerSuit(), nil
}

func (server *Server) ShuffleCards(gameId string) error {
	game, exists := server.games[gameId]
	if !exists {
		return newNotFoundError("game", gameId)
	}
	randomGenerator := server.randomGeneratorFactory()
	game.ShuffleDecks(randomGenerator)
	return nil
}
