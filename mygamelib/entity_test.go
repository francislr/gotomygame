package mygamelib

import (
	"fmt"
	"testing"
)

func TestDeck(t *testing.T) {
	cards := make([]*Card, 4)
	card1, _ := NewCard("1A")
	card2, _ := NewCard("5S")
	card3, _ := NewCard("10H")
	card4, _ := NewCard("1D")
	cards[0] = card1
	cards[1] = card2
	cards[2] = card3
	cards[3] = card4
	deck := NewDeck("1", cards)
	if len(deck.Cards()) != 4 {
		t.Errorf("expected 4 cards, got %d", len(deck.Cards()))
	}
}

func TestGame(t *testing.T) {
	game := NewGame("1", "Room")
	player := NewPlayer("1", "Player 1")
	game.AddPlayer(player)
	if len(game.Players()) != 1 {
		t.Errorf("expected 1 player, got %d", len(game.Players()))
	}

	err1 := game.AddPlayer(player)
	if err1 == nil {
		t.Errorf("expected error when player is already in game, got success")
	}

	game.RemovePlayer(player)
	if len(game.Players()) != 0 {
		t.Errorf("expected 0 player, got %d", len(game.Players()))
	}

	err2 := game.RemovePlayer(player)
	if err2 == nil {
		t.Errorf("expected error when player is not in game")
	}
}

func TestNewCard(t *testing.T) {
	testCard := func(code string, valid bool) {
		_, err := NewCard(code)

		if err == nil && !valid {
			t.Errorf("missing error for invalid card '%s'", code)
		}
		if err != nil && valid {
			t.Errorf("got error for valid card '%s'", code)
		}

	}

	testCard("0S", false)
	testCard("5R", false)
	testCard("1S", false)
	testCard("K0S", false)
	for _, rank := range CardRanks {
		for _, suit := range CardSuits {
			code := fmt.Sprintf("%s%s", rank, suit)
			testCard(code, true)
		}
	}
}

func TestCard(t *testing.T) {
	card1, err := NewCard("10H")
	if err != nil {
		t.Errorf("unexpected error '%s'", err)
	}
	if card1.Rank() != "10" {
		t.Errorf("unexpected rank '%s'", card1.Rank())
	}
	if card1.Suit() != "H" {
		t.Errorf("unexpected suit '%s'", card1.Suit())
	}
	if card1.Value() != 10 {
		t.Errorf("unexpected value '%d'", card1.Value())
	}
	card2, err := NewCard("JD")
	if err != nil {
		t.Errorf("unexpected error '%s'", err)
	}
	if card2.Rank() != "J" {
		t.Errorf("unexpected rank '%s'", card2.Rank())
	}
	if card2.Suit() != "D" {
		t.Errorf("unexpected suit '%s'", card2.Suit())
	}
	if card2.Value() != 11 {
		t.Errorf("unexpected value '%d'", card2.Value())
	}
	card3, err := NewCard("AC")
	if err != nil {
		t.Errorf("unexpected error '%s'", err)
	}
	if card3.Rank() != "A" {
		t.Errorf("unexpected rank '%s'", card3.Rank())
	}
	if card3.Suit() != "C" {
		t.Errorf("unexpected suit '%s'", card3.Suit())
	}
	if card3.Value() != 1 {
		t.Errorf("unexpected value '%d'", card3.Value())
	}

}
