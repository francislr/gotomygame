package mygamelib

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
)

func buildRouter() *gin.Engine {
	router := gin.Default()
	server := NewServer(func() *rand.Rand {
		return rand.New(rand.NewSource(0))
	})
	server.InitializeRoutes(router)
	return router
}

func sendHttpRequest(router *gin.Engine, method string, url string, body []byte) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, url, bytes.NewBuffer(body))
	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)
	return w
}

func TestGameLifecycleHandler(t *testing.T) {
	router := buildRouter()
	body, _ := json.Marshal(&CreateGameRequest{
		Name: "Room Name",
	})

	testResourceLifecycle(t, router, "/games/100", body)
}

func TestPlayerLifecycleHandler(t *testing.T) {
	router := buildRouter()
	body, _ := json.Marshal(&CreatePlayerRequest{
		Name: "Player Name",
	})
	testResourceLifecycle(t, router, "/players/101", body)
}

func TestCreateDeckHandler(t *testing.T) {
	router := buildRouter()
	testResourceCreation(t, router, "/decks/102", nil)
}

func TestAddDeckToGameHandler(t *testing.T) {
	router := buildRouter()
	body, _ := json.Marshal(&CreateGameRequest{
		Name: "Room Name",
	})
	sendHttpRequest(router, "POST", "/games/100", body)
	sendHttpRequest(router, "POST", "/games/105", body)
	sendHttpRequest(router, "POST", "/decks/102", body)
	w1 := sendHttpRequest(router, "POST", "/games/100/decks/102", nil)
	if http.StatusNoContent != w1.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusNoContent, w1.Code)
	}
	// Add deck to a second game
	w2 := sendHttpRequest(router, "POST", "/games/105/decks/102", body)
	if http.StatusBadRequest != w2.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusBadRequest, w2.Code)
	}
}

func TestAddPlayerToGameHandler(t *testing.T) {
	router := buildRouter()
	gameBody, _ := json.Marshal(&CreateGameRequest{
		Name: "Room Name",
	})
	playerBody, _ := json.Marshal(&CreatePlayerRequest{
		Name: "Player Name",
	})
	sendHttpRequest(router, "POST", "/games/100", gameBody)
	sendHttpRequest(router, "POST", "/games/105", gameBody)
	sendHttpRequest(router, "POST", "/players/102", playerBody)
	w1 := sendHttpRequest(router, "POST", "/games/100/players/102", nil)
	if http.StatusNoContent != w1.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusNoContent, w1.Code)
	}
	// Add player to a second game
	w2 := sendHttpRequest(router, "POST", "/games/105/players/102", nil)
	if http.StatusNoContent != w2.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusNoContent, w2.Code)
	}
	// Delete player from a game
	w3 := sendHttpRequest(router, "DELETE", "/games/100/players/102", nil)
	if http.StatusNoContent != w3.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusNoContent, w3.Code)
	}
	w4 := sendHttpRequest(router, "DELETE", "/games/100/players/102", nil)
	if http.StatusBadRequest != w4.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusBadRequest, w4.Code)
	}
}

func TestDealCardsHandler(t *testing.T) {
	router := buildRouter()
	gameBody, _ := json.Marshal(&CreateGameRequest{
		Name: "Room Name",
	})
	playerBody, _ := json.Marshal(&CreatePlayerRequest{
		Name: "Player Name",
	})
	dealCardBody, _ := json.Marshal(&DealCardRequest{
		Count: 52,
	})
	sendHttpRequest(router, "POST", "/games/100", gameBody)
	sendHttpRequest(router, "POST", "/players/101", playerBody)
	sendHttpRequest(router, "POST", "/decks/102", nil)
	sendHttpRequest(router, "POST", "/games/100/players/101", nil)
	sendHttpRequest(router, "POST", "/games/100/decks/102", nil)
	w1 := sendHttpRequest(router, "POST", "/games/100/players/101/deal-cards", dealCardBody)
	if http.StatusNoContent != w1.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusNoContent, w1.Code)
	}
	w2 := sendHttpRequest(router, "POST", "/games/100/players/101/deal-cards", dealCardBody)
	if http.StatusNoContent != w2.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusNoContent, w2.Code)
	}
}

func TestPlayerCardsHandler(t *testing.T) {
	router := buildRouter()
	gameBody, _ := json.Marshal(&CreateGameRequest{
		Name: "Room Name",
	})
	playerBody, _ := json.Marshal(&CreatePlayerRequest{
		Name: "Player Name",
	})
	dealCardBody, _ := json.Marshal(&DealCardRequest{
		Count: 52,
	})
	sendHttpRequest(router, "POST", "/games/100", gameBody)
	sendHttpRequest(router, "POST", "/players/101", playerBody)
	sendHttpRequest(router, "POST", "/decks/102", nil)
	sendHttpRequest(router, "POST", "/games/100/players/101", nil)
	sendHttpRequest(router, "POST", "/games/100/decks/102", nil)
	sendHttpRequest(router, "POST", "/games/100/players/101/deal-cards", dealCardBody)
	w1 := sendHttpRequest(router, "GET", "/games/100/players/101/cards", nil)
	getPlayerCardResponse1 := GetPlayerCardResponse{}
	json.NewDecoder(w1.Body).Decode(&getPlayerCardResponse1)

	if len(getPlayerCardResponse1.Cards) != 52 {
		t.Errorf("expected %d cards got %d", 52, len(getPlayerCardResponse1.Cards))
	}
	w2 := sendHttpRequest(router, "GET", "/games/100/players/101/cards", nil)
	getPlayerCardResponse2 := GetPlayerCardResponse{}
	json.NewDecoder(w2.Body).Decode(&getPlayerCardResponse2)

	if len(getPlayerCardResponse2.Cards) != 52 {
		t.Errorf("expected %d cards got %d", 52, len(getPlayerCardResponse2.Cards))
	}
}

func TestGetPlayerListHandler(t *testing.T) {
	router := buildRouter()
	gameBody, _ := json.Marshal(&CreateGameRequest{
		Name: "Room Name",
	})
	playerBody, _ := json.Marshal(&CreatePlayerRequest{
		Name: "Player Name",
	})
	dealCardBody, _ := json.Marshal(&DealCardRequest{
		Count: 10,
	})
	sendHttpRequest(router, "POST", "/games/100", gameBody)
	sendHttpRequest(router, "POST", "/players/101", playerBody)
	sendHttpRequest(router, "POST", "/players/102", playerBody)
	sendHttpRequest(router, "POST", "/decks/103", nil)
	sendHttpRequest(router, "POST", "/games/100/players/101", nil)
	sendHttpRequest(router, "POST", "/games/100/players/102", nil)
	sendHttpRequest(router, "POST", "/games/100/decks/103", nil)
	sendHttpRequest(router, "POST", "/games/100/players/101/deal-cards", dealCardBody)
	sendHttpRequest(router, "POST", "/games/100/players/102/deal-cards", dealCardBody)
	w1 := sendHttpRequest(router, "GET", "/games/100/players", nil)
	getPlayerListResponse := GetPlayerListResponse{}
	json.NewDecoder(w1.Body).Decode(&getPlayerListResponse)

	if len(getPlayerListResponse.Players) != 2 {
		t.Errorf("expected 2 players got %d", len(getPlayerListResponse.Players))
	}
}

func TestCountUndealtPerSuitHandler(t *testing.T) {
	router := buildRouter()
	gameBody, _ := json.Marshal(&CreateGameRequest{
		Name: "Room Name",
	})
	playerBody, _ := json.Marshal(&CreatePlayerRequest{
		Name: "Player Name",
	})
	sendHttpRequest(router, "POST", "/games/100", gameBody)
	sendHttpRequest(router, "POST", "/players/101", playerBody)
	sendHttpRequest(router, "POST", "/decks/103", nil)
	sendHttpRequest(router, "POST", "/games/100/players/101", nil)
	sendHttpRequest(router, "POST", "/games/100/decks/103", nil)
	w1 := sendHttpRequest(router, "GET", "/games/100/count-undealt-per-suit", nil)
	countPerSuitResponse := CountPerSuitResponse{}
	json.NewDecoder(w1.Body).Decode(&countPerSuitResponse)

	for _, suit := range []string{"C", "D", "H", "S"} {
		if countPerSuitResponse.CountPerSuit[suit] != 13 {
			t.Errorf("expected 13 cards for suit %s got %d", suit, countPerSuitResponse.CountPerSuit[suit])
		}

	}
}

func TestCountUndealtPerSuitAndRankHandler(t *testing.T) {
	router := buildRouter()
	gameBody, _ := json.Marshal(&CreateGameRequest{
		Name: "Room Name",
	})
	playerBody, _ := json.Marshal(&CreatePlayerRequest{
		Name: "Player Name",
	})
	sendHttpRequest(router, "POST", "/games/100", gameBody)
	sendHttpRequest(router, "POST", "/players/101", playerBody)
	sendHttpRequest(router, "POST", "/decks/103", nil)
	sendHttpRequest(router, "POST", "/games/100/players/101", nil)
	sendHttpRequest(router, "POST", "/games/100/decks/103", nil)
	w1 := sendHttpRequest(router, "GET", "/games/100/count-undealt-cards", nil)
	countCardResponse := CountCardResponse{}
	json.NewDecoder(w1.Body).Decode(&countCardResponse)

	if len(countCardResponse.CardCount) != 52 {
		t.Errorf("expected 52 cards got %d", len(countCardResponse.CardCount))
	}

	i := 0
	for _, suit := range []string{"H", "S", "C", "D"} {
		for _, rank := range []string{"K", "Q", "J", "10", "9", "8", "7", "6", "5", "4", "3", "2", "A"} {
			code := fmt.Sprintf("%s%s", rank, suit)
			if countCardResponse.CardCount[i].Card != code {
				t.Errorf("expected card %s at index %d, got %s", code, i, countCardResponse.CardCount[i].Card)
			}
			i++
		}
	}
}

// Utility function to test resource creation
func testResourceCreation(t *testing.T, router *gin.Engine, url string, body []byte) {
	w1 := sendHttpRequest(router, "POST", url, body)
	if http.StatusCreated != w1.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusCreated, w1.Code)
	}
	// Second POST request must fail
	w2 := sendHttpRequest(router, "POST", url, body)
	if http.StatusConflict != w2.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusConflict, w2.Code)
	}
}

func testResourceLifecycle(t *testing.T, router *gin.Engine, url string, body []byte) {
	testResourceCreation(t, router, url, body)
	w2 := sendHttpRequest(router, "DELETE", url, body)
	if http.StatusNoContent != w2.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusNoContent, w2.Code)
	}
	// Delete non-existing
	w3 := sendHttpRequest(router, "DELETE", url, body)
	if http.StatusNotFound != w3.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusNotFound, w3.Code)
	}
	// Second PUT request must not fail
	w4 := sendHttpRequest(router, "PUT", url, body)
	if http.StatusCreated != w4.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusCreated, w4.Code)
	}
	w6 := sendHttpRequest(router, "PUT", url, body)
	if http.StatusCreated != w6.Code {
		t.Errorf("unexpected http response code, expected %d got %d", http.StatusCreated, w6.Code)
	}
}
