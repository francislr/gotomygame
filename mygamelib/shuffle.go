package mygamelib

import (
	rng "math/rand"
)

func (deck *Deck) Shuffle(randomGenerator *rng.Rand) {
	sourceCards := deck.cards
	newCardOrder := make([]*Card, len(sourceCards))

	i := 0
	for len(sourceCards) > 0 {
		cardIndex := 0
		if len(sourceCards) > 1 {
			cardIndex = randomGenerator.Intn(len(sourceCards))
		}
		randomCard := sourceCards[cardIndex]
		// Remove card from list
		sourceCards = append(sourceCards[:cardIndex], sourceCards[cardIndex+1:]...)
		newCardOrder[i] = randomCard
		i++
	}
	deck.cards = newCardOrder
}
