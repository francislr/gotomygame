package main

import (
	"math/rand"

	"github.com/gin-gonic/gin"
	"gitlab.com/francislr/gotomygame/mygamelib"
)

func main() {
	randomGeneratorFactory := func() *rand.Rand {
		return rand.New(&mygamelib.CryptoSource64{})
	}
	server := mygamelib.NewServer(randomGeneratorFactory)
	router := gin.Default()
	server.InitializeRoutes(router.Group("/api/v1"))

	router.Run("localhost:8080")
}
