Basic Deck of Cards Game
========================

This repository contains the code for an assignment named "A Basic Deck of Cards Game". The API specification is described below.

## Card Codes
Each card is represented with a code. The code is a string in the following format:

```
<rank><suit>
```

where
* `<rank>` is a number or a letter for face cards:
  * J: Jack
  * Q: Queen
  * K: King
  * A: Ace
* `<suit>` is a letter:
  * H: ♥ Hearts
  * S: ♠ Spades
  * C: ♣ Clubs
  * D: ♦ Diamonds

## API Specification

### Create a game

**URL** : `/api/v1/games/:gameId`

**Method** : `POST`, `PUT`

**Body** :
```json
{
    "name": "A room name"
}
```

#### Success Response
**Code** : `201 Created`

#### Error Responses
**Code** :
`409 Conflict` The game already exists.

### Delete a game

**URL** : `/api/v1/games/:gameId`

**Method** : `DELETE`

#### Success Response
**Code** : `204 No Content`

#### Error Responses
**Code** : `404 Not Found` The game does not exist.

### Create a player

**URL** : `/api/v1/players/:playerId`

**Method** : `POST`, `PUT`

**Body** :
```json
{
    "name": "A player name"
}
```

#### Success Response
**Code** : `201 Created`

#### Error Responses
**Code** :
`409 Conflict` The player already exists.

### Delete a player

**URL** : `/api/v1/players/:playerId`

**Method** : `DELETE`

#### Success Response
**Code** : `204 No Content`

#### Error Responses
**Code** : `404 Not Found` The player does not exist.

### Create a deck

**URL** : `/api/v1/decks/:deckId`

**Method** : `POST`, `PUT`

#### Success Response
**Code** : `201 Created`

#### Error Responses
**Code** :
`409 Conflict` The deck already exists.

### Add a player to a game

**URL** : `/api/v1/games/:gameId/players/:playerId`

**Method** : `POST`

#### Success Response
**Code** : `204 No Content`

#### Error Responses
**Code** : `404 Not Found` The game or player does not exist.

### Remove a player from a game

**URL** : `/api/v1/games/:gameId/players/:playerId`

**Method** : `DELETE`

#### Success Response
**Code** : `204 No Content`

#### Error Responses
**Code** : `404 Not Found` The game or player does not exist.

### Add a deck to a game

**URL** : `/api/v1/games/:gameId/decks/:deckId`

**Method** : `POST`

#### Success Response
**Code** : `204 No Content`

#### Error Responses
**Code** : `404 Not Found` The game or player does not exist.

**Code** : `400 Bad Request` The deck is already assigned to a game.

### Deal cards to a player

**URL** : `/api/v1/games/:gameId/players/:playerId/deal-cards`

**Method** : `POST`

**Body** :
The parameter `count` is the number of cards dealt to the player.
```json
{
    "count": 1
}
```

#### Success Response
**Code** : `204 No Content`

#### Error Responses
**Code** : `400 Not Found` The game or player does not exist.

**Code** : `400 Bad Request` Invalid JSON

**Code** : `400 Bad Request` The player is not in the game

### Get the list of cards for a player
Return a list of card codes which belongs to the player in the game.

**URL** : `/api/v1/games/:gameId/players/:playerId/cards`

**Method** : `GET`

#### Success Response
**Code** : `200 OK`

**Body** :

```json
{
    "cards": [
        "AD",
        "AC"
    ]
}
```

#### Error Responses
**Code** : `404 Not Found` The game or player does not exist.

### Get the list of players in a game
Return a list of players. The total added value of all cards is included for each player entry. The list is ordered by summation.

**URL** : `/api/v1/games/:gameId/players`

**Method** : `GET`

#### Success Response
**Code** : `200 OK`

**Body** :

```json
{
    "players": [
        {
            "id": "1002",
            "name": "Player 1",
            "totalCardValue": 80
        },
                {
            "id": "1003",
            "name": "Player 2",
            "totalCardValue": 10
        }
    ]
}
```

#### Error Responses
**Code** : `404 Not Found` The game does not exist.

### Count undealt cards per suit

Get the count of how many cards per suit are left undealt in the game deck.

**URL** : `/api/v1/games/:gameId/count-undealt-per-suit`

**Method** : `GET`

#### Success Response
**Code** : `200 OK`

**Body** :

```json
{
    "countPerSuit": {
        "C": 23,
        "D": 23,
        "H": 24,
        "S": 24
    }
}
```

#### Error Responses
**Code** : `404 Not Found` The game does not exist.

### Count undealt cards per suit and rank

**URL** : `/api/v1/games/:gameId/count-undealt-cards`

**Method** : `GET`

#### Success Response
**Code** : `200 OK`

**Body** :

Get the count of how many cards per suit are left undealt in the game deck.

```json
{
    "countPerSuit": {
        "C": 23,
        "D": 23,
        "H": 24,
        "S": 24
    }
}
```

#### Error Responses
**Code** : `404 Not Found` The game does not exist.

### Shuffle all undealt cards

Shuffle all decks added to game.

**URL** : `/api/v1/games/:gameId/shuffle-cards`

**Method** : `POST`

#### Success Response
**Code** : `204 No Content`

#### Error Responses
**Code** : `404 Not Found` The game does not exist.
